# README #

### What is this repository for? ###

* LED control server
* LED control web app
* [Demo Movie](https://youtu.be/OZJddWoyyRY)

### How do I get set up? ###

* Install raspbian on raspberry pi
* Setup node.js
* Install [pi-blaster](https://github.com/sarfata/pi-blaster)
* Clone this repository
* Move to cloned directory and install library:
```
npm install
```
* Run server:
```
node server.js
```
