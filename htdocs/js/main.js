function DeviceMotionToColor(webSocketConnection){
    var on = true;
    var pos_x = 0;
    var pos_y = 0;
    var pos_z = 0;
    var pos_x_target = $('#pos-x');
    var pos_y_target = $('#pos-y');
    var pos_z_target = $('#pos-z');
    var color_target = $('#color');
    var body = $('body');

    function iosHandleOrientation(event) {
        var orientData = event.accelerationIncludingGravity;
        var accel_scale = 1.0;
        var filter_val = 0.1;

        // ローパスフィルタ
        pos_x = (orientData.x*accel_scale * filter_val) + (pos_x * (1.0 - filter_val));
        pos_y = (-orientData.y*accel_scale * filter_val) + (pos_y * (1.0 - filter_val));
        pos_z = (-orientData.z*accel_scale * filter_val) + (pos_z * (1.0 - filter_val));
        pos_x_target.text('x = ' + pos_x.toFixed(5));
        pos_y_target.text('y = ' + pos_y.toFixed(5));
        pos_z_target.text('z = ' + pos_z.toFixed(5));

        var red = ((pos_x + 9.8) / 9.8 / 2).toFixed(3);
        if (red < 0) red = 0;
        if (1.0 < red) red = 1.0;
        var green = ((pos_y + 9.8) / 9.8 / 2).toFixed(3);
        if (green < 0) green = 0;
        if (1.0 < green) green = 1.0;
        var blue = ((pos_z + 9.8) / 9.8 / 2).toFixed(3);
        if (blue < 0) blue = 0;
        if (1.0 < blue) blue = 1.0;

        var data = {
            red: red, green: green, blue: blue
        };
        if (!on) {
            data = {red: 0, green: 0, blue: 0};
        }

        var bgColor = '#' +
            ('0' + (Math.floor(data.red * 255)).toString(16)).substr(-2) +
            ('0' + (Math.floor(data.green * 255)).toString(16)).substr(-2) +
            ('0' + (Math.floor(data.blue * 255)).toString(16)).substr(-2);
        body.css('background-color', bgColor);
        color_target.text(bgColor);

        throttledSendData(data);
    }

    function sendData(data) {
        webSocketConnection.send(0, data.red, data.green, data.blue);
    }
    var throttledSendData = _.throttle(sendData, 100);

    $('#bt-on').click(function(){
        on = true;
    });

    $('#bt-off').click(function(){
        on = false;
    });

    this.start = function(){
        window.addEventListener("devicemotion", iosHandleOrientation, true);
    };

    this.stop = function(){
        window.removeEventListener('devicemotion', iosHandleOrientation);
    };
}

$(function() {
    var webSocketConnection = new WebSocketConnection();
    var deviceMotionToColor = new DeviceMotionToColor(webSocketConnection);
    deviceMotionToColor.start();
    $('#wrapper').click(function(){
        $('#display').toggle();
    });
});