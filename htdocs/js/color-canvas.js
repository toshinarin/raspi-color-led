// http://stackoverflow.com/questions/7812514/drawing-a-dot-on-html5-canvas
// http://www.html5canvastutorials.com/labs/html5-canvas-color-picker/

function ColorCanvas(canvasId, colorPreviewId) {
    var _this;
    var canvas = document.getElementById(canvasId);
    var ctx = canvas.getContext("2d");

    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;
    var canvasTop = canvas.offsetTop;
    var canvasLeft = canvas.offsetLeft;
    var canvasData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);

    var isDragging = false;
    var selectedColorDiv = document.getElementById(colorPreviewId);
    canvas.addEventListener('mousedown', function(e){
        isDragging = true;
        applyColorWithClick(e);
    });
    canvas.addEventListener('mouseup', function(e){
        isDragging = false;
    });
    canvas.addEventListener('mousemove', function(e){
        if (isDragging) {
            applyColorWithClick(e);
        }
    });

    canvas.addEventListener('touchstart', function(e){
        isDragging = true;
        applyColorWithTouch(e);
    });
    canvas.addEventListener('touchend', function(e){
        isDragging = false;
    });
    canvas.addEventListener('touchmove', function(e){
        if (isDragging) {
            applyColorWithTouch(e);
        }
    });

    function applyColorWithClick(e) {
        selectedColorDiv.style.backgroundColor = getPixelColor(e.offsetX, e.offsetY);
        if (_this && typeof _this.onColorSelected == 'function') {
            _this.onColorSelected(getPixel(e.offsetX, e.offsetY));
        }
    }

    function applyColorWithTouch(e) {
        e.preventDefault();
        e.stopPropagation();
        var touch = e.touches[0];
        var x = touch.pageX - canvasLeft;
        var y = touch.pageY - canvasTop;
        selectedColorDiv.style.backgroundColor = getPixelColor(x, y);
        if (_this && typeof _this.onColorSelected == 'function') {
            _this.onColorSelected(getPixel(x, y));
        }
    }

    function drawPixel (x, y, r, g, b, a) {
        var index = (x + y * canvasWidth) * 4;

        canvasData.data[index + 0] = r;
        canvasData.data[index + 1] = g;
        canvasData.data[index + 2] = b;
        canvasData.data[index + 3] = a;
    }

    function updateCanvas() {
        ctx.putImageData(canvasData, 0, 0);
    }

    function getPixel (x, y) {
        var index = (x + y * canvasWidth) * 4;
        return {
            r: canvasData.data[index + 0],
            g: canvasData.data[index + 1],
            b: canvasData.data[index + 2],
            a: canvasData.data[index + 3]
        };
    }

    function getPixelColor (x, y) {
        var index = (x + y * canvasWidth) * 4;
        return '#' +
            ('0' + canvasData.data[index + 0].toString(16)).substr(-2) +
            ('0' + canvasData.data[index + 1].toString(16)).substr(-2) +
            ('0' + canvasData.data[index + 2].toString(16)).substr(-2);
    }


    function hsvToRGB(h, s, v) {
        var r = v;
        var g = v;
        var b = v;
        if (0 < s) {
            h *= 6.0;
            const i = Math.floor(h);
            const f = h - i;
            switch (i) {
                default:
                case 0:
                    g *= 1 - s * (1 - f);
                    b *= 1 - s;
                    break;
                case 1:
                    r *= 1 - s * f;
                    b *= 1 - s;
                    break;
                case 2:
                    r *= 1 - s;
                    b *= 1 - s * (1 - f);
                    break;
                case 3:
                    r *= 1 - s;
                    g *= 1 - s * f;
                    break;
                case 4:
                    r *= 1 - s * (1 - f);
                    g *= 1 - s;
                    break;
                case 5:
                    g *= 1 - s;
                    b *= 1 - s * f;
                    break;
            }
        }
        return {
            r: r,
            g: g,
            b: b
        };
    }

    this.init = function(){
        _this = this;
        canvasWidth = canvas.width;
        canvasHeight = canvas.height;
        if (canvasHeight % 2 === 1) {
            canvasHeight += 1;
        }
        canvasTop = canvas.offsetTop;
        canvasLeft = canvas.offsetLeft;
        canvasData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);

        for (var i = 0; i < canvasWidth; i++) {
            for (var j = 0; j <= canvasHeight / 2; j++) {
                var h = i / canvasWidth;
                var s = (canvasHeight / 2 - j) / (canvasHeight / 2);
                var v = 1.0;
                var rgb = hsvToRGB(h, s, v);
                drawPixel(i, canvasHeight / 2 - j, rgb.r * 255, rgb.g * 255, rgb.b * 255, 255);
                var rgb2 = hsvToRGB(h, s, s);
                drawPixel(i, j + canvasHeight / 2 - 1, rgb2.r * 255, rgb2.g * 255, rgb2.b * 255, 255);
            }
        }

        updateCanvas();
    };

    this.onColorSelected = function(){

    };
}


