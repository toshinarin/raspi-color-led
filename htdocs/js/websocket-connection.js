function WebSocketConnection(){
    var url = location.hostname;
    var wsport = 3100;
    var isConnected = false;

    var ws = new WebSocket('ws://' + url + ':' + wsport);
    ws.onopen = function(){
        isConnected = true;
    };
    ws.onclose = function(){
        isConnected = false;
    };

    this.send = function(id, red, green, blue){
        if (!isConnected){
            return;
        }

        var jsonobj = {
            light: [
                {id: id, red: red, green: green, blue: blue}
            ]
        };
        var json = JSON.stringify(jsonobj);
        ws.send(json);
    };
}
