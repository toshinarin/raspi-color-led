var hosturl = '0.0.0.0';
var wsport = 3100;

var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({host: hosturl, port: wsport});

var pb = require('pi-blaster.js');
var gpio = {r: 23, g: 24, b: 25};
function light(data){
    pb.setPwm(gpio.r, data.red);
    pb.setPwm(gpio.g, data.green);
    pb.setPwm(gpio.b, data.blue);
}

var connect = require('connect');
var http = require('http');
var app = connect();
var serveStatic = require('serve-static');
app.use(serveStatic('htdocs', {'index': 'index.html'}));
http.createServer(app).listen(3000);

wss.on('connection', function(ws) {
    ws.on('message', function(message) {
        console.log(message);
        var mes = JSON.parse(message);
        light(mes.light[0]);
    });
});
